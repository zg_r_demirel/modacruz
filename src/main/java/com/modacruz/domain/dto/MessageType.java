package com.modacruz.domain.dto;

/**
 * Created by ozgur.demirel on 12/24/2015.
 */
public enum MessageType {
    SUCCESS, INFO, WARNING, ERROR
}