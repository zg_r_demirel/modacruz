package com.modacruz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/***
 * hata mesajlarını yakalayana dto daha spesifik olunabilir.
 * proje scopu icin yeterlidir
 */
@Data
@AllArgsConstructor
public class MessageDTO {
    private String message;
    private MessageType type;
    public MessageDTO() {
    }
}
