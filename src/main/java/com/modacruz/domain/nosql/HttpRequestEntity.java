package com.modacruz.domain.nosql;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "httplogs")
public class HttpRequestEntity implements Serializable{

	@Id
	@Indexed
	private String id;
	private String url;
	private String type;
	private String body;
	private String response;
	private Date creationdate;
}
