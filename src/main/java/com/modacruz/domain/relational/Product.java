package com.modacruz.domain.relational;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"version"})
@Table(name = "PRODUCT")
public class Product extends BaseDataClass implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "PRODUCT_NAME")
    @NotNull(message = "productName.can.not.be.null")
    private String productName;//urun adı
    @Column(name = "PRODUCT_DESCRIPTION")
    @NotNull(message = "productDescription.can.not.be.null")
    private String productDescription;//urun aciklamasi
    @Column(name="PRODUCT_PRICE")
    @NotNull(message = "productPrice.can.not.be.null")
    private Double productPrice;//urun fiyati
    @Column(name="PRODUCT_COLOUR")
    private String colour; //urun renk enum olabilir
    @Column(name="PRODUCT_SIZE")
    private String size; //beden enum olabilir
    @Version
    @Column(name = "VERSION")
    private Integer version;
}
