package com.modacruz.controller;

import com.modacruz.domain.relational.Product;
import com.modacruz.service.respository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@RestController
@Slf4j
public class ProductController {
    //Post methodlar  : create resources
    //Put methodlar  : update resources

    @Autowired
    private ProductRepository productRepository;


    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Product create(@Validated @RequestBody Product product) {
        Product save = productRepository.save(product);
        return save;
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Product get(@PathVariable("id") Long id) {
        log.info("get product id is : "+ id);
        Product product = productRepository.findOne(id);
        if (product == null) {
            throw new EntityNotFoundException("cant found any product on system");
        }
        return product;
    }


//    @RequestMapping("/echo/{echo}")
//    private String echo(@PathVariable("echo") String echo) {
//        return echo + "!";
//    }

}
