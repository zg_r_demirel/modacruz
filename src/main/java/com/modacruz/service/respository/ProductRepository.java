package com.modacruz.service.respository;

import com.modacruz.domain.relational.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ozgur.demirel on 12/23/2015.
 */
//@RepositoryRestResource(path = "/uygunUrl") //istenirse aynen açılabiliyor rest olarak
public interface ProductRepository extends JpaRepository<Product, Long> {
    Product save(Product product);

    Product findOne(Long id);

    long count();
}
