package com.modacruz.service.respository;

import com.modacruz.domain.nosql.HttpRequestEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HttpRequestEntityRepository extends MongoRepository<HttpRequestEntity, String> {
    HttpRequestEntity save(HttpRequestEntity entity);

}
