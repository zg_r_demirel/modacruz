
-- application yaml da ddl i create yaparak oluşturdugum postgresql tablosudur.
create TABLE product (
  id bigint not null,
  product_colour varchar(255),
  product_description varchar(255) not null,
  product_name varchar(255) not null,
  product_price double precision not null,
  product_size varchar(255),
  version integer,
  PRIMARY KEY (id)
);
CREATE UNIQUE INDEX product_pkey ON product (id);