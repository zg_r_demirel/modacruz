package com.modacruz.service.repository;

import com.modacruz.ModaCruzApplication;
import com.modacruz.domain.nosql.HttpRequestEntity;
import com.modacruz.service.respository.HttpRequestEntityRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest(randomPort = true)
@ActiveProfiles({"prod,mongo"})
@SpringApplicationConfiguration(classes = ModaCruzApplication.class)
public class HttpRequestEntityMongoRepositoryTest {

    @Autowired
    private HttpRequestEntityRepository httpRequestEntityRepository;

    @Test
    public void testWriteHttpEntity() throws Exception {
        HttpRequestEntity entity=new HttpRequestEntity();
        entity.setType("ops type");
        entity.setUrl("ops url");
        entity.setBody("ops body");
        entity.setResponse("ops reponse");
        entity.setCreationdate(new Date());
        HttpRequestEntity save = httpRequestEntityRepository.save(entity);
        Assert.assertNotNull(save);
    }
}
