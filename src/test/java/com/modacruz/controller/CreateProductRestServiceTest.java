package com.modacruz.controller;

import com.modacruz.domain.dto.MessageDTO;
import com.modacruz.domain.dto.MessageType;
import com.modacruz.domain.relational.Product;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

@Slf4j
public class CreateProductRestServiceTest extends BaseRestIntegrationTest {


    @Test
    public void createServiceSuccessTest() {
        Product product = new Product();
        product.setColour("red"+System.currentTimeMillis());
        product.setProductDescription("test description");
        product.setProductName("test productName");
        product.setProductPrice(124D);
        product.setSize("small");
        Product result = restTemplate.postForObject(host + "/create", product, Product.class);
        Assert.assertNotNull(" product id can not be null after persisted product",result.getId());
    }

    @Test
    public void createServiceValidationProductPriceTest()  {
        Product product = new Product();
        product.setColour("red");
        product.setProductDescription("test description");
        product.setProductName("test productName");
        // product.setProductPrice(124D); excluded
        product.setSize("small");
        //---
        MessageDTO messageDTO = restTemplate.postForObject(host + "/create", product, MessageDTO.class);
        log.info(messageDTO.toString());
        Assert.assertNotNull(messageDTO.getType().name().equals(MessageType.ERROR.name()));
    }

    @Test
    public void createServiceValidationProductNameTest()  {
        Product product = new Product();
        product.setColour("red");
        product.setProductDescription("test description");
        //product.setProductName("test productName");//excluded
        product.setProductPrice(124D);
        product.setSize("small");
        //---
        MessageDTO messageDTO = restTemplate.postForObject(host + "/create", product, MessageDTO.class);
        log.info(messageDTO.toString());
        Assert.assertNotNull(messageDTO.getType().name().equals(MessageType.ERROR.name()));
    }

    @Test
    public void createServiceValidationProductDescriptionTest()  {
        Product product = new Product();
        product.setColour("red");
        //product.setProductDescription("test description");
        product.setProductName("test productName");//excluded
        product.setProductPrice(124D);
        product.setSize("small");
        //---
        MessageDTO messageDTO = restTemplate.postForObject(host + "/create", product, MessageDTO.class);
        log.info(messageDTO.toString());
        Assert.assertNotNull(messageDTO.getType().name().equals(MessageType.ERROR.name()));
    }

//    private String generateProductJson(Product product) throws JsonProcessingException {
//        return (new ObjectMapper()).writeValueAsString(product);
//    }

}
