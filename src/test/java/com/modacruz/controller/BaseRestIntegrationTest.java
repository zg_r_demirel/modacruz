package com.modacruz.controller;

import com.modacruz.ModaCruzApplication;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@WebIntegrationTest(randomPort = true)
@ActiveProfiles({"prod"})
@SpringApplicationConfiguration(classes = ModaCruzApplication.class)
public abstract class BaseRestIntegrationTest {

    @Value("${local.server.port}")
    private Integer serverPort;

    public RestTemplate restTemplate;
    public String host;

    @Before
    public void setUpMvcFields() throws Exception {
        restTemplate=new RestTemplate();
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                //hata fırlatmaması için handle error methodunu override ediyorum.
                //exception nesnesini test edebilmek için iptal ettim.
            }
        });
        host = "http://localhost:"+serverPort;
    }

}
