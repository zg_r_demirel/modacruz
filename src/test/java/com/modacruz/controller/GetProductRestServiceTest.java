package com.modacruz.controller;

import com.modacruz.domain.dto.MessageDTO;
import com.modacruz.domain.relational.Product;
import com.modacruz.service.respository.ProductRepository;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Slf4j
public class GetProductRestServiceTest extends BaseRestIntegrationTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void testGetServiceSuccessTest() throws Exception {
        Long idFormGetService = getRandomProductIdFormGetService();
        if (idFormGetService==null) return;
        Map<String, Long> params = new HashMap<>();
        params.put("id", idFormGetService);
        Product product = restTemplate.getForObject(host + "/get/{id}", Product.class, params);
        log.info(product.getId()+" : "+product.getProductName());
        Assert.assertNotNull(product.getId());
    }

    @Test
    public void testGetServiceFailTest()  {
        //NotFoundException mesajını döner
        Long idFormGetService = 0L;
        Map<String, Long> params = new HashMap<>();
        params.put("id", idFormGetService);
        MessageDTO messageDTO = restTemplate.getForObject(host + "/get/{id}", MessageDTO.class, params);
        log.info(messageDTO.getMessage()+" : "+messageDTO.getType().name());
        Assert.assertNotNull(messageDTO);
    }


    private Long getRandomProductIdFormGetService(){
        long count = productRepository.count();
        if (count == 0) return null;
        List<Product> all = productRepository.findAll();
        Random rand = new Random();
        Integer i = rand.nextInt(Integer.valueOf(count + ""));
        return  all.get(i).getId();
    }

}
